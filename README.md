# Gulp Task Manager Test

Because why not? :D On your end, this should be really, really simple, assuming all works correctly. You should not need to know anything about Gulp or task managers in at all to get going. (I certainly did not when I first worked on a project that used one when making an open-source contribution.) You won't need to touch the Gulp files at all. Just the normal HTMl, Sass, and JS files.

## What's the point of this? 

The best thing about it is **live reload**. That means once you're up and running (with a few simple commands), all changes you save to your Sass and JS files will be reflected immediately in the browser without having to refresh. 

In addition, it's going to make it a lot easier to keep things consistent across all of our files ("style" of coding), it's going to catch any syntax errors and rule-breaking immediately before we try to run our code, and it's going to make our endpoint files *really* really tiny and fast for out site to load (and imaginary users of our framework). 

## What do you need to do / know?

Pretty much nothing.

### Making sure you have what you need

1. First, you will need to make sure you have NodeJS installed. If you don't have it, you can [download it here][node-dl].

    To check if you have it installed already, run the following command:  

    `node -v`    

    If you have Node installed, you will see something like this:  

    `v7.6.0` (or whatever version number you have)  

2. npm (Node Package Manager) should be installed automatically when Node installs, but just to make sure, run the following command:  

    `npm -v`  

    If you see something like `4.1.2`, you're good to go. If you get an error, try to troubleshoot via their really helpful [documentation][npm-doc].  

### Now, it's time to get to work

Now, you're ready to clone the repo and get going.

1. Run the following command:  

    `git clone https://justasterisms@bitbucket.org/justasterisms/testing-gulp-installs.git`  

    and then make sure you `cd` into the repo's directory:

    `cd testing-gulp-installs`

    *NOTE:* This is just for testing purposes to make sure everything installs and works right on your end.

1. Now you will install all of the dependencies (files Gulp needs to do its job). Run the following command:

    `npm install`  

    **If you get an error saying Gulp was not found, run:**  

    `npm install gulp`
    
    followed by `npm install` once again

1. To set up the local server (with live reload in your browser) and compile your Sass and JS when you save the files, run:
  
    `gulp`  

    Whenever you need to "bust out" of it to do something like Git stuff, just hit <kbd>CTRL</kbd> + <kbd>C</kbd>. To restart the server and compile, just run `gulp` again. 


## Accessing the HTML pages on the local server

After running the `gulp` command, a local server will start up and this is what will allow us to see the changes in real time. You will see something like this on the command line:

`Local: http://localhost:3000` (Yours will likely be on a different port, so don't worry about the number.)

That is the URL we will use to access our site. 

Our site lives in the `site` directory. This will be the "home" directory for our server. It's where `index.html` (the page you see when it first loads) lives. To access other pages, just treat the end of the url in your browser like you would any other file structure.

For example, to access `site/demo.html`, when my local server is spinning, I'd go to `http://localhost:3000/demo.html`. 

This is just a test repo I set up to test my build file, so ignore the files and structure. I just want to make sure everything I wrote about above works for everyone without errors.

To test out the live reload feature, try to edit the header text in `site/index.html` while the local server is running, and then save the file!


## Any questions or concerns?

I know it's hard for us to get on Slack at the same time and everyone is busy, but I check my DMs on Slack often, so if any of you have questions, issues, or concerns about using this for our framework, let me know! 


[node-dl]: https://nodejs.org/en/download/
[npm-doc]: https://docs.npmjs.com/troubleshooting/try-the-latest-stable-version-of-node