var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var cssmin = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var prefix = require('gulp-autoprefixer');
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var size = require('gulp-size');
var scssLint = require('stylelint-scss');
var jshint = require('gulp-jshint');


// sets up live reloading
gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: "site"
        }
    });
});

// sets up SCSS linting
gulp.task('sass-lint', function () {
  gulp.src('scss/**/*.scss')
    .pipe(scssLint())
    .pipe(scssLint.format())
    .pipe(scssLint.failOnError());
});

// takes all components SCSS, compiles it,
// gzips it, adds prefixes, minifies, adds .min to end of name
gulp.task('framework-scss', function() {
  return gulp.src('src/scss/**/*.scss')
    .pipe(sass())
    .pipe(size({ gzip: true, showFiles: true }))
    .pipe(prefix())
    .pipe(gulp.dest('src/css'))
    .pipe(cssmin())
    .pipe(size({ gzip: true, showFiles: true }))
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('src/css'))
    .pipe(gulp.dest('site/css'))
    .pipe(reload({stream:true}));
});

// takes all site SCSS, compiles it,
// gzips it, adds prefixes, minifies, adds .min to end of name
gulp.task('site-scss', function() {
  return gulp.src('site/scss/**/*.scss')
    .pipe(sass())
    .pipe(size({ gzip: true, showFiles: true }))
    .pipe(prefix())
    .pipe(gulp.dest('site/css'))
    .pipe(cssmin())
    .pipe(size({ gzip: true, showFiles: true }))
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('site/css'))
    .pipe(reload({stream:true}));
});


// lints JS code for errors and going against best practices
gulp.task('jshint', function() {
  gulp.src('src/js/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

// Takes all JS files from the src directory concatenates them into one file
// gzips it, then minifies that file
gulp.task('js-squash', function() {
  return gulp.src(['./src/js/components/*.js', './src/js/*.js'])
    .pipe(concat('scripts.js'))
    .pipe(size({ gzip: true, showFiles: true }))
    .pipe(uglify())
    .pipe(size({ gzip: true, showFiles: true }))
    .pipe(rename({ suffix: '.min'}))
    .pipe(gulp.dest('site'));
});

// makes sure everything is compiled and refreshed when a file is saved
gulp.task('watch', function() {
  gulp.watch('src/scss/**/*.scss', ['framework-scss', 'site-scss', 'sass-lint']);
  gulp.watch('site/scss/**/*.scss', ['site-scss', 'sass-lint']);
  gulp.watch('src/js/**/*.js', ['jshint']); 
  gulp.watch('site/**/*.html', browserSync.reload);
  gulp.watch('src/js/**/*.js', browserSync.reload); 
});

// starts Browser Sync, sets up handling of Sass, and sets up JS linting handling
// when gulp is run
gulp.task('default', ['browser-sync', 'framework-scss', 'site-scss', 'js-squash', 'watch']);